# install.pp
define restrictedshell::shell (
  $ensure = 'present',
  $path = '/usr/local/bin',
  $template = 'restrictedshell/restricted.sh.erb',
  $commands = []
) {

  file { "${path}/${name}":
    ensure  => $ensure,
    path    => "${path}/${name}",
    content => template($template),
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
  }
}
