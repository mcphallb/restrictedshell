# init.pp
class restrictedshell (
  $hiera = false,
  $shells = {}
) {

  if $hiera {
    validate_hash( $shells )
    create_resources('restrictedshell::shell',$shells)
  }
}
